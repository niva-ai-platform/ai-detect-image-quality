from detect_image_quality import detect_image_quality
from ai_python_detect_wrapper_library import AiDetectWrapper
from config import detect_image_quality_args

ai_detect_wrapper = AiDetectWrapper()
ai_detect_wrapper.register_service()
ai_detect_wrapper.create_download_dir()
ai_detect_wrapper.initialise_app(detect_image_quality, detect_image_quality_args)

app = ai_detect_wrapper.get_app()
