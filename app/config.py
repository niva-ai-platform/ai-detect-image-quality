detect_image_quality_args = {
    'upper_brightness_threshold': 155,
    'lower_brightness_threshold': 75,
    'blur_threshold': 100,
    'resolution_threshold': 480 * 360
}
