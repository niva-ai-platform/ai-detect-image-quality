from typing import Any
import cv2
import numpy as np


def detect_image_quality(file_in: str,
                         confidence_threshold: float,
                         upper_brightness_threshold: float,
                         lower_brightness_threshold: float,
                         blur_threshold: float,
                         resolution_threshold: int) -> dict[str, Any]:

    image = cv2.imread(file_in, cv2.IMREAD_GRAYSCALE)
    tags = []

    average_brightness = image.mean()
    if average_brightness > upper_brightness_threshold:
        tags.append('over-exposed')
    elif average_brightness < lower_brightness_threshold:
        tags.append('under-exposed')

    # percentage_above_threshold = percentage_pixels_above_luminance_threshold(
    #     image, UPPER_LUMINANCE_THRESHOLD)
    # if percentage_above_threshold > UPPER_PERCENTAGE_THRESHOLD:
    #     print('Too bright:', filename, percentage_above_threshold)

    # percentage_below_threshold = percentage_pixels_below_luminance_threshold(
    #     image, LOWER_LUMINANCE_THRESHOLD)
    # if percentage_below_threshold > LOWER_PERCENTAGE_THRESHOLD:
    #     print('Too dark:', filename, percentage_below_threshold)

    blurriness = cv2.Laplacian(image, cv2.CV_64F).var()
    if blurriness < blur_threshold:
        tags.append('blurry')

    h, w = image.shape[:2]
    pixels = h * w
    if pixels < resolution_threshold:
        tags.append('low-resolution')

    detections = {
        'brightness': {
            'is_over_exposed': 'over-exposed' in tags,
            'is_under_exposed': 'under-exposed' in tags,
            'average': average_brightness,
            'upper_threshold': upper_brightness_threshold,
            'lower_threshold': lower_brightness_threshold,
        },
        'blur': {
            'is_blurry': 'blurry' in tags,
            'blurriness': blurriness,
            'threshold': blur_threshold
        },
        'resolution': {
            'is_low_resolution': 'low-resolution' in tags,
            'width': w,
            'height': h,
            'threshold': resolution_threshold
        }
    }

    output_data = {
        'tags': tags,
        'results': {
            'targetDetected': bool(tags),
            'maxConfidence': int(bool(tags)),
            'detections': detections
        }
    }

    return output_data


def percentage_pixels_below_luminance_threshold(image: cv2.Mat, threshold: float):
    number_of_pixels = image.size
    count_below = np.sum(image < threshold)
    return count_below / number_of_pixels


def percentage_pixels_above_luminance_threshold(image: cv2.Mat, threshold: float):
    number_of_pixels = image.size
    count_above = np.sum(image > threshold)
    return count_above / number_of_pixels
