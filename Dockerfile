FROM python:3.10

ARG GITBRANCH="local"
ARG GITSHA="local"

WORKDIR /app

COPY ./requirements.txt /app/requirements.txt
RUN pip install -r requirements.txt

COPY ./requirements-walton.txt /app/requirements-walton.txt
RUN pip install -r requirements-walton.txt

# COPY ./requirements-frozen.txt /app/requirements-frozen.txt
# RUN pip install --no-deps -r /app/requirements-frozen.txt

COPY ./app /app

LABEL gitbranch=${GITBRANCH}
LABEL gitsha=${GITSHA}

ENV GITBRANCH=${GITBRANCH}
ENV GITSHA=${GITSHA}

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]
