import sys
sys.path.append('../app')

from app.detect_image_quality import detect_image_quality
from os import listdir
from os.path import isfile, join

# IMAGE_PATH = './images/Clean Permanent Pasture'
IMAGE_PATH = './images/Other'
# IMAGE_PATH = './images/Bare Soil'
# IMAGE_PATH = './images/sample_images'

UPPER_BRIGHTNESS_THRESHOLD = 155
LOWER_BRIGHTNESS_THRESHOLD = 75
BLUR_THRESHOLD = 100
RESOLUTION_THRESHOLD = 480 * 360

# UPPER_LUMINANCE_THRESHOLD = 160
# LOWER_LUMINANCE_THRESHOLD = 60
# UPPER_PERCENTAGE_THRESHOLD = 0.5 # 0.445
# LOWER_PERCENTAGE_THRESHOLD = 0.5

sample_images = [join(IMAGE_PATH, image_name) for image_name in listdir(IMAGE_PATH)
    if isfile(join(IMAGE_PATH, image_name)) and image_name != '.DS_Store']

for filename in sample_images:
    output = detect_image_quality(filename, 0, UPPER_BRIGHTNESS_THRESHOLD, LOWER_BRIGHTNESS_THRESHOLD, BLUR_THRESHOLD, RESOLUTION_THRESHOLD)
    if 'low-resolution' in output['tags']:
        print(filename)